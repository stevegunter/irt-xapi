This is a README file for the Estimating Learner Ability with Item Response Theory paper presented at I/ITSEC, December 2021. The paper was co-authored by Stephen Gunter, Ph.D. and Jeffrey M. Beaubien, Ph.D. from Aptima, Inc. 

The `code` in this directory is for demonstration purposes. This is the exact `code` that was used for the paper to: 
- Simulate learner responses from an IRT model
- Develop simulated xAPI statements for synthetic learners
- Parse xAPI statements for the necessary IRT data
- Estimate learner abilities from the simulated data

If you have any comments, questions, or concerns about the `code` or IRT please contact Stephen Gunter, Ph.D. at sgunter@aptima.com 
